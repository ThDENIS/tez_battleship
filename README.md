BATAILLE NAVALE

Règles du jeu :
i. Deux joueurs, chaque joueur a une flotte composée de 5 bateaux (cases, champ de tir) :
	a. Porte-avion (5, 2),
	b. Croiseur (4, 2)
	c. Contre torpilleur (3, 2)
	d. Sous-marin (3, 4)
	e. Torpilleur (2, 5)

ii. Un tir est limité à un champ de tir vertical ou horizontal. (Défini dans la classe des bateaux
)
iii. Après un tir raté, l’adversaire déplace un bateau jusqu’à deux cases.

iv. Un bateau touché plus que 2 fois, est détruit.



Universite du Quebec A Chicoutimi

Auteur :
	- @zoe0807
	- Etienne G
	- @ThDENIS 

Date : 
	- 27/SEPT/18

Langage : 
	- Java


package model;

import java.util.ArrayList;

import model.Enum;

public abstract class Boat {
	
	protected int size; 
	protected int life; 
	protected Coordinate front; //Partie la plus en haut  gauche sur le plateau
	protected Enum.Orientation orientation; //Orientation (Verticale ou Horizontale) du bateau 
	protected int rangeShot; 
	protected Enum.Orientation orientationShot; 
	

	public Boat(int size, Coordinate front, Enum.Orientation orientation, int rangeShot, Enum.Orientation orientationShot) {
		this.size = size; 
		this.life = 3; 
		this.front = front; 
		this.orientation = orientation; 
		this.rangeShot = rangeShot; 
		this.orientationShot = orientationShot; 
	}
	
	
	/*
	 * Getters et Setters
	 */
	
	public int getSize() {
		return this.size; 
	}
	
	public void setSize(int size) {
		this.size = size; 
	}
	
	public int getLife() {
		return life; 
	}
	
	public void setLife(int life) {
		this.life = life; 
	}
	
	public Coordinate getFront() {
		return this.front;
	}
	
	public void setFront(Coordinate front) {
		this.front = front; 
	}
	
	public Enum.Orientation getOrientation() {
		return this.orientation; 
	}
	
	public void setOrientation(Enum.Orientation orientation) {
		this.orientation = orientation; 
	}
	
	public int getRangeShot() {
		return this.rangeShot;
	}
	
	public void setRangeShot(int rangeShot) {
		this.rangeShot = rangeShot; 
	}
	
	public Enum.Orientation getOrientationShot(){
		return this.orientationShot; 
	}
	
	public void setOrientationShot(Enum.Orientation orientationShot) {
		this.orientationShot = orientationShot;
	}
	
	

	/**
	 * Fonction permettant de renvoyer si le bateau est contenu dans cette case
	 * @param coordinate
	 * @return
	 */
	public boolean containsBoat(Coordinate coordinate) {
		
		if (orientation == Enum.Orientation.Vertical)
			return (front.getLetter() == coordinate.getLetter() && this.front.getNumber() <= coordinate.getNumber() && coordinate.getNumber() <= getBack().getNumber()); 
		else 
			return (front.getNumber() == coordinate.getNumber() && this.front.getLetter() <= coordinate.getLetter() && coordinate.getLetter() <= getBack().getLetter());
	}
	
	
	/**
	 * Fonction permettant de connaitre le joueur  qui appartient le bateau
	 * @param board
	 * @return
	 */
	public int getPlayer(Board board) {
		for (int i=0; i<board.getPlayers().length; i++ ) {
			for (int b=0; b<board.getPlayers()[i].getFleet().length; b++) {
				if (this == board.getPlayers()[i].getFleet()[b])
					return i;
			}
		}
		return -1; 
	}
	
	/**
	 * Fonction permettant de creer un bateau, et le renvoie. On renvoie null lorsque le bateau cree n'est pas dans le plateau
	 * @param board
	 * @param front
	 * @param orientation
	 * @param boat
	 * @return
	 */
	public static Boat createBoat(Board board, Coordinate front, Enum.Orientation orientation, Enum.BoatType boat) {
		Boat newboat;
		switch(boat) {
			case AircraftCarrier: newboat = new AircraftCarrier(front, orientation); break;
			case Cuiser: newboat = new Cuiser(front, orientation); break;
			case Destroyer: newboat = new Destroyer(front, orientation); break;
			case Submarin: newboat = new Submarin(front, orientation); break;
			case TorpedoBoat: newboat = new TorpedoBoat(front, orientation); break;
			default : newboat = new Submarin(front, orientation); break;
		}
		
		// Verification du placement du bateau (on regarde si l'avant et l'arriere sont bien sur le plateau)
		if(front.isValid() && newboat.getBack().isValid())
			return newboat;
		else
			return null;
	}
	
	/**
	 * Fonction permettant d'avoir les coordonnees de l'arriere du bateau
	 * @return
	 */
	public Coordinate getBack() {
			Coordinate back; 
			
			if (orientation == Enum.Orientation.Vertical)
					back = new Coordinate(this.front.getLetter(), this.front.getNumber()+size-1);
			else 
					back = new Coordinate(this.front.getLetterIndex()+size-1, this.front.getNumber());
			return back; 
	}
	

	

	/**
	 * Fonction permettant au bateau de se deplacer
	 * @param board
	 * @param direction
	 * @param distance
	 * @return
	 */
	public boolean move(Board board, Enum.Direction direction, int distance) {
		if (!this.isAlive())
			return false; 
		
		Enum.Case player = (getPlayer(board) == 0 ? Enum.Case.P1 : Enum.Case.P2);
		ArrayList<Coordinate> coordBoatToVerify = new ArrayList<Coordinate>();
		
		if ((orientation == Enum.Orientation.Vertical && direction == Enum.Direction.Up) || (orientation == Enum.Orientation.Horizontal && direction == Enum.Direction.Left ))
			coordBoatToVerify.add(front);
		else if ((orientation == Enum.Orientation.Vertical && direction == Enum.Direction.Down) || (orientation == Enum.Orientation.Horizontal && direction == Enum.Direction.Right ))
			coordBoatToVerify.add(getBack());
		else //Dans ce cas on doit tester toutes les cases du bateau 
			coordBoatToVerify = getAllCoords(); 
		for (Coordinate coord : coordBoatToVerify) {
			for (int i=1; i<=distance; i++) {
				Coordinate next_coord =  coord.nextCoordinate(board, direction, i);
				System.out.println();
				
				if (next_coord == null) {
					
					return false; 
				}
				else if (board.getCase(next_coord) == player || board.getCase(next_coord) == Enum.Case.Both) {
					
					return false; 
				}
			}		
		}
		front = front.nextCoordinate(board, direction, distance);
		return true;
	}
		
	
	/**
	 * Fonction permettant de donner les coordonnees des cases qui peuvent etre visees lorsqu'on veut effectuer un tir
	 * @param board
	 * @return
	 */
	public ArrayList<Coordinate> getShotTargets(Board board) {
		ArrayList<Coordinate> shotTargets = new ArrayList<Coordinate>(); 
		if (orientationShot == Enum.Orientation.Vertical) {
			if (orientation == Enum.Orientation.Vertical) {
				for (int i=1; i<=rangeShot; i++) {
					Coordinate coord_front = front.nextCoordinate(board, Enum.Direction.Up, i);
					Coordinate coord_back = getBack().nextCoordinate(board, Enum.Direction.Down, i);
					if (coord_front != null) 
						shotTargets.add(coord_front);
					if (coord_back != null)
						shotTargets.add(coord_back);
				}
			}
			else { //Cas du bateau qui est  l'horizontal 
				for (int i=1; i<=rangeShot; i++) {
					ArrayList<Coordinate> boatCoords = getAllCoords(); 
					for (Coordinate coord : boatCoords) {
						Coordinate coord_up = coord.nextCoordinate(board, Enum.Direction.Up, i); 
						if (coord_up != null)
							shotTargets.add(coord_up); 
						Coordinate coord_down = coord.nextCoordinate(board, Enum.Direction.Down, i); 
						if (coord_down != null)
							shotTargets.add(coord_down); 
						
					}
				}
			}
		}
		else { //Tir du bateau horizontal 
			if (orientation == Enum.Orientation.Horizontal) {
				for (int i=1; i<=rangeShot; i++) {
					Coordinate coord_front = front.nextCoordinate(board, Enum.Direction.Left, i);
					Coordinate coord_back = getBack().nextCoordinate(board, Enum.Direction.Right, i);
					if (coord_front != null) 
						shotTargets.add(coord_front);
					if (coord_back != null)
						shotTargets.add(coord_back);
				}
			}
			else {
				for (int i=1; i<=rangeShot; i++) {
					ArrayList<Coordinate> boatCoords = getAllCoords(); 
					for (Coordinate coord : boatCoords) {
						Coordinate coord_left = coord.nextCoordinate(board, Enum.Direction.Left, i); 
						if (coord_left != null)
							shotTargets.add(coord_left); 
						
						Coordinate coord_right = coord.nextCoordinate(board, Enum.Direction.Right, i); 
						if (coord_right != null)
							shotTargets.add(coord_right); 
						
					}
				}
			}
		}
		return shotTargets; 	
	}
	
	/**
	 * Fonction permettant au bateau de tirer, on verifie si le tir est valide, et s'il touche un bateau, le coule ou le manque
	 * @param board
	 * @param coord
	 * @return
	 */
	public Enum.ShotResult shot(Board board, Coordinate coord) {
				
		Enum.Case opponent = (getPlayer(board) == 0 ? Enum.Case.P2 : Enum.Case.P1);
		int opponent_player = (getPlayer(board) == 0 ? 1 : 0);
		Enum.ShotResult shotResult ; 
		if (this.getShotTargets(board).contains(coord)) {
			Enum.Case board_case = board.getCase(coord);
			if (board_case == Enum.Case.Both || board_case == opponent) {
				Boat b = board.getBoat(coord, opponent_player);
				if (b.isAlive()) {
					b.setLife(b.getLife()-1);
					if (b.getLife() == 0)
						return Enum.ShotResult.Sank;
					else 
						return Enum.ShotResult.Hit;
				}
				else 
					return Enum.ShotResult.Miss;
			}
			else
				return Enum.ShotResult.Miss;
		}
		else
			return Enum.ShotResult.Invalid;
	}
	
	
	/**
	 * Fonction qui renvoie vrai si deux bateaux se superposent
	 * @return
	 */
	public static boolean conflict(Boat b1, Boat b2) {
		ArrayList<Coordinate> b1List = b1.getAllCoords();
		ArrayList<Coordinate> b2List = b2.getAllCoords();
		
		for(int i = 0; i < b1List.size(); i++) {
			if(b2List.contains(b1List.get(i)))
				return true;
		}
		return false;
	}

	/**
	 * Fonction permettant d'avoir une liste de toutes les coordonnees d'un bateau
	 * @return
	 */
	public ArrayList<Coordinate> getAllCoords () {
		
		ArrayList<Coordinate> listCoords = new ArrayList<>();
		listCoords.add(front);
		Coordinate last = front;
		// parcours du reste du bateau
		for(int i = 0; i < size-1; i++) {
			if(this.orientation == Enum.Orientation.Horizontal)
				last = new Coordinate(last.getLetterIndex()+1, last.getNumber());
			else
				last = new Coordinate(last.getLetter(), last.getNumber()+1);
			listCoords.add(last);
		}
		return listCoords;
	}
	
	/**
	 * Fonction permettant de savoir si le bateau est "en vie", et donc s'il n'est pas coule
	 * @return
	 */
	public boolean isAlive() {
		if (life == 0)
			return false; 
		else 
			return true;
	}
		
}

package model;

public class Coordinate {
	
	private char letter; 
	private int number; 
	private static String alphabet = "ABCDEFGHIJ";
	
	public Coordinate(char letter, int number) {
		this.letter = letter; 
		this.number = number; 
	}
	
	public Coordinate(int letter, int number) {
		if (0 < letter && letter <= 10) {
			this.letter = alphabet.charAt(letter-1); 
		}
		else 
			this.letter = 'Z';
		
		this.number = number; 
	}
	
	/*
	 * Getters et Setters
	 */
	public char getLetter() {
		return letter; 
	}
	
	public void setLetter(char letter) {
		this.letter = letter; 
	}
	
	public int getNumber() {
		return number; 
	}
	
	public void setNumber(int number) {
		this.number = number; 
	}

	@Override
	public String toString() {
		return "Coordinate [letter=" + letter + ", number=" + number + "]";
	}
	
	/**
	 * Override de la methode equals pour faire fonctionner correctement la methode Contains d'ArrayList
	 */
	@Override
	public boolean equals(Object object)
	{	
		if (object == null) {
            return false;
        }
		
		final Coordinate coord = (Coordinate) object;
		return coord.letter == this.letter && coord.number == this.number;
	}
	
	
	/**
	 * Fonction permettant de verifier qu'une coordonnee appartient bien au plateau 
	 * @return
	 */
	public boolean isValid() {
		return letter >= 'A' && letter <= 'J' && number >= 1 && number <= 10;
	}
	
	/**
	 * Fonction permettant de calculer la coordonnee qui se trouve a une distance et une direction donnee, et de la renvoyer uniquement si elle est valide
	 * @param board
	 * @param direction
	 * @param distance
	 * @return
	 */
	public Coordinate nextCoordinate(Board board, Enum.Direction direction, int distance) {
		Coordinate coord; 
		switch (direction) {
			case Up : 
				coord = new Coordinate(letter, number-distance);
				if (!coord.isValid()) 
					return null;
				break; 
			case Down : 
				coord = new Coordinate(letter, number+distance);
				if (!coord.isValid()) 
					return null;
				break;
			case Left :
				coord = new Coordinate(getLetterIndex()-distance, number);
				if (!coord.isValid()) 
					return null;
				break;
			case Right : 
				coord = new Coordinate(getLetterIndex()+distance, number);
				if (!coord.isValid()) 
					return null;
				break;
			default : 
				coord = null; 
		}
		if (coord.isValid())	
			return coord; 
		else 
			return null;
	}
	
	/** 
	 * Fonction permettant de donner le nombre correspondant a la lettre dans les coordonnees
	 * @return
	 */
	public int getLetterIndex() {
		return alphabet.indexOf(letter)+1;
				
	}
	
	
}

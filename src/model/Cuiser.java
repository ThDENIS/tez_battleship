package model;

public class Cuiser extends Boat{
	
	private static final int size = 4 ;
	private static final int rangeShot = 2; 
	private static final Enum.Orientation orientationShot = Enum.Orientation.Horizontal ; 
	
	public Cuiser(Coordinate front, Enum.Orientation orientation) {
		super(size, front, orientation, rangeShot, orientationShot);
	}
}

package model;

public class Player {
	
	private String name; 
	private Boat[] fleet; 

	public Player(String name) {
		this.name = name; 
		this.fleet = new Boat[5]; 
	}
	
	/*
	 * Getters et Setters
	 */
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name; 
	}
	
	public Boat[] getFleet() {
		return this.fleet; 
	}
	
	public void setFleet(Boat[] fleet) {
		this.fleet = fleet;
	}
	
	/**
	 * Fonction permettant d'ajouter un bateau a la flotte d'un joueur
	 * @param boat
	 * @return
	 */
	public boolean addBoat(Boat boat) {
		int index = 0;
		for(Boat currentBoat : this.fleet) {
			if(currentBoat == null) {
				if(checkNewBoat(boat)) {
					this.fleet[index] = boat;
					return true;
				}
				return false;
			}
			index++;
		}
		return true;
	}
	
	
	/**
	 * Verifie si le nouveau bateau touche un autre
	 * @param boat
	 * @return
	 */
	public boolean checkNewBoat(Boat boat) {
		for(Boat currentBoat : this.fleet) {
			if(currentBoat == null)
				break;
			if(Boat.conflict(boat, currentBoat))
				return false;
		}
		return true;
	}
	
	/**
	 * Fonction permettant de compter le nombre de bateaux en vie dans la flotte du joueur
	 * @return
	 */
	public int remainingBoats() {
		int cpt = 0;
		for(Boat boat : fleet) {
			if(boat.life > 0)
				cpt++;
		}
		return cpt;
	}

}

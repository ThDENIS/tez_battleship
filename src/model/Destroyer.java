package model;

public class Destroyer extends Boat{
	private static final int size = 3;
	private static final int rangeShot = 2; 
	private static final Enum.Orientation orientationShot = Enum.Orientation.Vertical ; 
	
	public Destroyer(Coordinate front, Enum.Orientation orientation) {
		super(size, front, orientation, rangeShot, orientationShot);
	}
}


package model;

import java.util.ArrayList;

public class Board {
	
	private static final int size = 10; 
	private Player[] players; 
	private int turn = 0; // Afin de dterminer le joueur qui joue
	
	public Board() {
		
		this.players = new Player[2];
		this.players[0] = new Player("player1");
		this.players[1] = new Player("player2");
		
	}
	
	public Board(Player[] players) {
		this.players = players;
	}
	
	public Board(Player p1, Player p2) {
		this.players = new Player[2];
		this.players[0] = p1; 
		this.players[1] = p2; 
	}
	
	/*
	 *Getters et Setters
	 */
	public int getSize() {
		return size; 
	}
	
	public Player[] getPlayers() {
		return this.players; 
	}
	
	public void setPlayers(Player[] players) {
		this.players = players; 
	}
	
	
	
	/**
	 * Fonction permettant d'obtenir ce qu'il y a sur la case (le bateau d'un joueur, des deux ou vide)
	 * @param coordinate
	 * @return
	 */
	public Enum.Case getCase(Coordinate coordinate){
		int cpt = 0; 
		for (int p=0; p<this.players.length; p++) {
			Player player = this.players[p];
			for (int b=0; b<player.getFleet().length; b++) {
				if (player.getFleet()[b] != null) {
					if (player.getFleet()[b].containsBoat(coordinate))
						cpt = (cpt == 0 ? p+1 : 3); 
				}
			}
		}
		switch (cpt) {
			case 0 : 
				return Enum.Case.Empty;
			case 1 : 
				return Enum.Case.P1;
			case 2 : 
				return Enum.Case.P2;
			case 3 : 
				return Enum.Case.Both; 
			default : 
				return Enum.Case.Empty;
		}
	}
	
	
	/**
	 * Fonction permettant de renvoyer le bateau contenu sur une case pour un joueur donnee
	 * @param coord
	 * @param player
	 * @return
	 */
	public Boat getBoat(Coordinate coord, int player) {
		for (int i=0; i<players[player].getFleet().length; i++) {
			if (players[player].getFleet()[i].getAllCoords().contains(coord))
				return players[player].getFleet()[i];
		}
		return null; 
	}
	
	/**
	 * Fonction permettant de savoir quel est le joueur gagnant quand la partie est terminee
	 * @return
	 */
	public Player getWinner() {
		for(int i = 0; i < players.length; i++) {
			if(players[i].remainingBoats() == 0)
				return players[(i+1)%2];		
		}
		return null;
	}
	
	/**
	 * Fonction permettant de savoir quand la partie est terminee (quand tous les bateaux d'un joueur ont coule)
	 * @return
	 */
	public boolean gameOver() {
		for(Player player : players) {
			if(player.remainingBoats() == 0)
				return true;
		}
		return false;
	}
	
	/**
	 * Fonction permettant de changer le joueur qui doit jouer
	 */
	public void changePlayer() {
		turn = (turn + 1) % 2;
	}
	
	/**
	 * Fonction permettant de retourner le joueur dont c'est le tour
	 * @return
	 */
	public Player getCurrentPlayer() {
		return players[turn];
	}

	
}

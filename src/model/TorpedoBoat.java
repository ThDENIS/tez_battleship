package model;

public class TorpedoBoat extends Boat {
	
	private static final int size = 2;
	private static final int rangeShot = 5; 
	private static final Enum.Orientation orientationShot = Enum.Orientation.Vertical ; 
	
	public TorpedoBoat(Coordinate front, Enum.Orientation orientation) {
		super(size, front, orientation, rangeShot, orientationShot);
	}

}

package model;

public class Submarin extends Boat{
	private static final int size = 3;
	private static final int rangeShot = 4; 
	private static final Enum.Orientation orientationShot = Enum.Orientation.Horizontal ; 
	
	public Submarin(Coordinate front, Enum.Orientation orientation) {
		super(size, front, orientation, rangeShot, orientationShot);
	}
}

package model;

public class Enum {
	
	public enum Case {
		Empty, P1, P2, Both;
	}
	
	public enum Orientation {
		Vertical, Horizontal
	}
	
	public enum Direction {
		Up, Down, Left, Right;
	}
	
	public enum BoatType {
		AircraftCarrier, Cuiser, Destroyer, Submarin, TorpedoBoat;
	}
	
	public enum ShotResult {
		Invalid, Miss, Hit, Sank; 
	}
	
	

}

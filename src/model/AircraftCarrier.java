package model;

public class AircraftCarrier extends Boat {
	
	private static final int size = 5;
	private static final int rangeShot = 2; 
	private static final Enum.Orientation orientationShot = Enum.Orientation.Vertical ; 
	
	public AircraftCarrier(Coordinate front, Enum.Orientation orientation) {
		super(size, front, orientation, rangeShot, orientationShot);
	}

	public int getSize() {
		return size;
	}

	public static int getRangeshot() {
		return rangeShot;
	}

	public static Enum.Orientation getOrientationshot() {
		return orientationShot;
	}
	
	
	
}
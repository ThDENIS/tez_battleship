//CHEVALLIER Zoe
//DENIS Thibault
//GUILLOU Etienne

/*
 * Regles du jeu 
 * Afin de determiner la facon de realiser notre bataille navale, nous avons fixe quelques regles : 
 * 		- Pour un joueur, il n'est pas possible d'avoir 2 bateaux sur une meme case.
 * 		- Le bateau peut tirer une case (ou plus suivant son champ de tir) devant lui, dans la direction fixee dans la classe. Il ne peut donc pas tirer sur lui-meme.
 * 		- Nous avons laisse la possibilite de tirer plusieurs fois sur la meme case lors d'un tour pour un joueur.
 */

package controller;

import model.Board;
import model.Boat;
import model.Coordinate;
import model.Enum.BoatType;
import model.Enum.Direction;
import model.Enum.Orientation;
import model.Enum.ShotResult;
import model.Player;
import view.Display;

public class Main {
	
	public static void main(String[] args) {

		BoatType[] boatsList = new BoatType[] { BoatType.AircraftCarrier, 
												BoatType.Cuiser, 
												BoatType.Destroyer, 
												BoatType.Submarin, 
												BoatType.TorpedoBoat };
		Boat boat;
		Coordinate coord;
		ShotResult shot;
		Direction direction; 
		int distance;
		boolean movement;

		
		// ---> Creation des joueurs
		Player[] players = Display.chooseName();
		Board board = new Board(players);
		
		// ---> Creation des bateaux pour chaque joueur
		for(Player currentPlayer : players) {
			System.out.println("----------------");
			Display.displayPlayerBoard(board, currentPlayer);
			
			for(int i = 0; i < boatsList.length; i++) {
				System.out.println("Placement des bateaux de " + currentPlayer.getName() + " : " + (5-i) + " restant(s)");
				Coordinate coordinate = Display.chooseCoord();
				Boat b = Boat.createBoat(board, coordinate, Display.chooseOrientation(coordinate), boatsList[i]);
				if(b != null && currentPlayer.addBoat(b)) { // Ajout du bateau 
					System.out.println("Bateau ajout en [" + coordinate.getLetter() + coordinate.getNumber() + "]");
					Display.displayPlayerBoard(board, currentPlayer);
				}
				else { // Il y a deja un bateau du joueur est place cet endroit, il faut le replacer
					System.out.println("-> Erreur ! Le bateau que vous voulez poser se superpose avec un autre de vos bateaux ou n'est pas sur la grille\n");
					i--;
				}
			}
		}
		
		
		while(!board.gameOver()) {
			
			System.out.println("Tour de jeu de " + board.getCurrentPlayer().getName());
			System.out.println("Voici votre plateau de jeu au debut du tour : ");
			Display.displayPlayerBoard(board, board.getCurrentPlayer());
			System.out.println("Vous pouvez maintenant tirer sur les bateaux de votre adversaire !");
			
			do {
				
				System.out.println("Pour vous aider, voici la liste des coordonnees atteignables par vos bateaux :");
				Display.displayBoatsRange(board, board.getCurrentPlayer());
				
				boat = Display.chooseBoat(board, board.getCurrentPlayer());
				coord = Display.choosePlacement();				
				shot = boat.shot(board, coord);
				System.out.println("Resultat du tir : " + shot);
				
				if (shot == ShotResult.Invalid)
					System.out.println("Veuillez entrer des coordonnees valides ! \n ");

			} while(shot != ShotResult.Miss && !board.gameOver());
			
			if (board.gameOver())
				break;
			
			System.out.println("Vous avez loupe votre cible, vous pouvez maintenant deplacer un de vos bateaux.");
			
			do {
				
				Display.displayPlayerBoard(board, board.getCurrentPlayer());
				boat = Display.chooseBoat(board, board.getCurrentPlayer()); 
				direction = Display.chooseDirection();
				distance = Display.chooseDistance(); 
				System.out.println("Direction " + direction + " - Distance : " + distance );
				movement = boat.move(board, direction, distance);
				if (!movement)
					System.out.println("Attention, ce deplacement n'est pas valide !");
				
			} while (!movement);
			
			System.out.println("Voici votre plateau a la fin de votre tour ! ");
			Display.displayPlayerBoard(board, board.getCurrentPlayer());
			board.changePlayer();

		}
		
		Display.endGame(board.getWinner());
	}
}
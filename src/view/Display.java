package view;

import java.util.Scanner;

import model.Board;
import model.Boat;
import model.Coordinate;
import model.Enum;
import model.Player;


public class Display {
	
	private static String alphabet = "ABCDEFGHIJ"; 
	private static Player p1, p2;

	/**
	 * Fonction permettant d'afficher les bateaux d'un joueur sur le plateau 
	 * @param board
	 * @param player
	 */
	public static void displayPlayerBoard(Board board, Player player) {
		System.out.println();
		
		Enum.Case case_player = (board.getPlayers()[0].equals(player) ? Enum.Case.P1 : Enum.Case.P2);
		int number_player = (board.getPlayers()[0].equals(player) ? 0 : 1);

		for (int i=0; i<board.getSize()+1; i++) {
			for (int j=0; j<board.getSize() + 1; j++) {
				System.out.print("\t");
				if (i>0 && j>0) {
					Coordinate coordinate = new Coordinate(j,i); 
					if (board.getCase(coordinate) == case_player || board.getCase(coordinate) == Enum.Case.Both) {
						Boat b = board.getBoat(coordinate, number_player);
						if (b.isAlive()) {
							String class_name = b.getClass().getSimpleName();
							switch (class_name) {
								case "AircraftCarrier" : 
									System.out.print("1");
									break; 
								case "Cuiser" : 
									System.out.print("2");
									break; 
								case "Destroyer" : 
									System.out.print("3");
									break; 
								case "Submarin" : 
									System.out.print("4");
									break; 
								case "TorpedoBoat" : 
									System.out.print("5");
									break; 
							}
						}
						else 
							System.out.print(".");

					}
					else
						System.out.print(".");
					
				}
				if (i == 0 && j < board.getSize())
					System.out.print(alphabet.charAt(j));
				if (j == 0 && i != 0)
					System.out.print("\n" + i);
			}
			if (i== 0 )
				System.out.println();
		}
		System.out.println();
		System.out.println();
		
		
	}
	
	/**
	 * Fonction permettant de creer les 2 joueurs
	 * @return
	 */
	public static Player[] chooseName() {
		System.out.println("Bonjour , quel est le nom du joueur 1 ?");
		Scanner sc = new Scanner(System.in);
		String name = sc.nextLine();
		p1 = new Player(name);
		System.out.println("Et , quel est le nom du joueur 2 ?");
		name = sc.nextLine();
		p2 = new Player(name);
		System.out.println("Bienvenue " + p1.getName() + " et " + p2.getName() + "!");
		return new Player[] {p1, p2};
	}
	
	/**
	 * Fonction permettant de choisir le placement d'un bateau 
	 * @return
	 */
	public static Coordinate choosePlacement() {
		System.out.println("Choisissez la coordonnee de depart de ce bateau (zone la plus en haut a gauche)");
		return chooseCoord();
	}
	
	/**
	 * Fonction permettant de recuperer les coordonnees saisies par l'utilisateur
	 * @return
	 */
	public static Coordinate chooseCoord() {
		Scanner sc = new Scanner(System.in);
		
		String letter, number;
		do {
			System.out.println("Choisir une lettre entre A et J pour la coordonnee");
			letter = sc.nextLine();
			letter = letter.toUpperCase();
			if(letter.length() != 1 || !Character.isLetter(letter.charAt(0)) || letter.charAt(0) > 'J')
				System.out.println("Erreur. Il faut rentrer une lettre entre A et J");
		} while(letter.length() != 1 || !Character.isLetter(letter.charAt(0)) || letter.charAt(0) > 'J');
		
		do {
			System.out.println("Choisir un chiffre entre 1 et 10 pour la coordonnee");
			number = sc.nextLine();
			if(number.length() != 1 && number.length() != 2 || !Character.isDigit(number.charAt(0)) || Integer.parseInt(number) > 10 || Integer.parseInt(number) < 1)
				System.out.println("Erreur. Il faut entrer un chiffre entre 1 et 10 pour terminer la coordonnee");
		} while(number.length() != 1 && number.length() != 2 || !Character.isDigit(number.charAt(0)) || Integer.parseInt(number) > 10 || Integer.parseInt(number) < 1);
		return new Coordinate(letter.charAt(0), Integer.parseInt(number));
	}
	
	/**
	 * Fonction permettant de recuprer l'orientation du bateau saisie par l'utilisateur
	 * @param coord
	 * @return
	 */
	public static Enum.Orientation chooseOrientation (Coordinate coord) {
		Scanner sc = new Scanner(System.in);
		String orientation;
		do {
			System.out.println("Choisir une orientation pour le bateau partant de [" + coord.getLetter() + coord.getNumber() + "] : V pour verticale et H pour horizontale");
			orientation = sc.nextLine();
			orientation = orientation.toUpperCase();		
			if(!orientation.equals("H") && !orientation.equals("V"))
				System.out.println("Erreur. Vous devez choisir V ou H pour l'orientation");
		} while(!orientation.equals("H") && !orientation.equals("V"));
		
		return orientation.equals("H") ? Enum.Orientation.Horizontal : Enum.Orientation.Vertical;
	}
	
	/**
	 * Fonction permettant de recuperer le bateau selectionne par l'utilisateur
	 * @param board
	 * @param player
	 * @return
	 */
	public static Boat chooseBoat(Board board, Player player) {
		Scanner sc = new Scanner(System.in);
		String boat;
		do {
			System.out.println("Choisissez un bateau (chiffre entre 1 et 5)" );
			boat = sc.nextLine();
			if(boat.length() != 1 || !Character.isDigit(boat.charAt(0)) || Integer.parseInt(boat) > 5 || Integer.parseInt(boat) < 1 || !player.getFleet()[Integer.parseInt(boat)-1].isAlive())
				System.out.println("Erreur. Vous devez choisir un numero de bateau parmi vos bateaux restants");
		} while(boat.length() != 1 || !Character.isDigit(boat.charAt(0)) || Integer.parseInt(boat) > 5 || Integer.parseInt(boat) < 1 || !player.getFleet()[Integer.parseInt(boat)-1].isAlive());
		
		return player.getFleet()[Integer.parseInt(boat)-1];
		
	}
	
	/**
	 * Fonction permettant de recuperer la direction saisie par l'utilisateur
	 * @return
	 */
	public static Enum.Direction chooseDirection() {
		Scanner sc = new Scanner(System.in);
		String direction;
		
		do {
			System.out.println("Choisissez une direction pour votre deplacement (U:Up, D:Down, R:Right, L:Left)");
			direction = sc.nextLine();
			if(direction.length() != 1 || !Character.isLetter(direction.charAt(0)) || (direction.charAt(0) != 'U' && direction.charAt(0) != 'D' && direction.charAt(0) != 'R' && direction.charAt(0) != 'L'))
				System.out.println("Erreur. Vous devez choisir une direction valide");
		} while(direction.length() != 1 || !Character.isLetter(direction.charAt(0)) || (direction.charAt(0) != 'U' && direction.charAt(0) != 'D' && direction.charAt(0) != 'R' && direction.charAt(0) != 'L'));
		switch (direction.charAt(0)) {
			case 'U': 
				return Enum.Direction.Up;
			case 'D' : 
				return Enum.Direction.Down; 
			case 'R' : 
				return Enum.Direction.Right; 
			case 'L' : 
				return Enum.Direction.Left;
			default : 
				return null;
		}		
	}
	
	/**
	 * Fonction permettant de recuperer la distance saisie par l'utilisateur
	 * @return
	 */
	public static int chooseDistance() {
		Scanner sc = new Scanner(System.in); 
		String distance; 
		
		do {
			System.out.println("Choisissez une distance pour votre deplacement : 1 ou 2");
			distance = sc.nextLine();
			if(distance.length() != 1 || !Character.isDigit(distance.charAt(0)) || Integer.parseInt(distance) < 1 || Integer.parseInt(distance) > 2)
				System.out.println("Erreur. La distance entree n'est pas valide");
		} while (distance.length() != 1 || !Character.isDigit(distance.charAt(0)) || Integer.parseInt(distance) < 1 || Integer.parseInt(distance) > 2); 
		
		return Integer.parseInt(distance);
	}
	
	/**
	 * Fonction permettant d'afficher les cases qui peuvent etre visees lors d'un tir pour un bateau
	 * @param board
	 * @param player
	 */
	public static void displayBoatsRange(Board board, Player player) {
		int cpt = 0;
		for(Boat boat : player.getFleet()) {
			if (boat.isAlive()) {
				System.out.print("Bateau " + (cpt+1) + " : ");
				for(Coordinate target : boat.getShotTargets(board)) {
					System.out.print(target.getLetter());
					System.out.print(target.getNumber() + " ");
				}
				System.out.println();
			}
			cpt++;
		}
	}
	
	/**
	 * Fonction permettant d'afficher le message de fin de partie
	 * @param player
	 */
	public static void endGame(Player player) {
		System.out.println("Partie terminee !");
		System.out.println("Vainqueur : " + player.getName() + " ! (" + player.remainingBoats() + "bateaux restants)");
	}
}
